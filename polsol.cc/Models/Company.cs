﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;

namespace polsol.cc.Models
{
    public enum CompanyCatagory
    {
        Cardinals
        ,Cubs
        ,Brewers
    }

    public class Company
    {
        public int ID { get; set; }
        [StringLength(50), Required]
        public string Name { get; set; }
        [StringLength(15), Required]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        public CompanyCatagory? Catagory { get; set; }

        public ICollection<Contact> Contacts { get; set; }

        [NotMapped]
        public string FormattedPhoneNumber {
            get
            {
                return Regex.Replace(PhoneNumber, @"(\d{3})(\d{3})(\d{4})", "$1-$2-$3");
            }
            set
            {
                PhoneNumber = new string(value.Where(c => char.IsDigit(c)).ToArray());
            }
        }
    }
}
