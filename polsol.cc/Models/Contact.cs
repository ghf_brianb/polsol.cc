﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace polsol.cc.Models
{
    public class Contact
    {
        public int ID { get; set; }
        [Display(Name = "Company"), Required(ErrorMessage = "The Company field is required")]
        public int CompanyId { get; set; }
        [StringLength(50)]
        [Display(Name = "First Name"), Required]
        public string FirstName { get; set; }
        [StringLength(50)]
        [Display(Name = "Last Name"), Required]
        public string LastName { get; set; }
        public Company Company { get; set; }

    }
}
