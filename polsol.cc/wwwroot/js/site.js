﻿// Write your JavaScript code.
$(document).ready(function () {

    // this sets the click event structure for company-table
    $("#company-table").delegate("tbody tr", "click", function () {
        // set row as selected
        $(this).addClass("selected").siblings().removeClass("selected");

        // get id of row to use for edit nav buttons
        var id = $(this).data("id");

        // set the edit buttons to active when a row is selected
        $("#company-edit-btn-group input").removeAttr('disabled');

        // loop buttons and append the id to the form action
        $("#company-edit-btn-group input").each(function () {
            $(this).attr("formaction", function (i, value) {
                var formaction = value.split("/");
                if (formaction.length > 3)
                {
                    return `${formaction[0]}/${formaction[1]}/${formaction[2]}/${id}`
                }
                else
                {
                    return `${value}/${id}`;
                }                
            });
        });
    });

    // this sets the click event structure for contact-table
    $("#contact-table").delegate("tbody tr", "click", function () {
        // set row as selected
        $(this).addClass("selected").siblings().removeClass("selected");

        // get id of row to use for edit nav buttons
        var id = $(this).data("id");

        // set the edit buttons to active when a row is selected
        $("#contact-edit-btn-group input").removeAttr('disabled');

        // loop buttons and append the id to the form action
        $("#contact-edit-btn-group input").each(function () {
            $(this).attr("formaction", function (i, value) {
                var formaction = value.split("/");
                if (formaction.length > 3) {
                    return `${formaction[0]}/${formaction[1]}/${formaction[2]}/${id}`
                }
                else {
                    return `${value}/${id}`;
                }
            });
        });
    });
});

