﻿using System.Linq;
using polsol.cc.Models; 

namespace polsol.cc.Data
{
    public class DbDataInitilizer
    {
        public static void Initialize(CompanyContext context)
        {
            context.Database.EnsureCreated();

            if (context.Companies.Any()) { return; }

            var companies = new Company[]
            {
                new Company{Name="All About Boo Co.", PhoneNumber="3141112222"},
                new Company{Name="Black Cat Crossroads", PhoneNumber="3123334444"},
                new Company{Name="Candy Corn Extraction Inc.", PhoneNumber="6185556666"}
            };

            foreach ( Company c in companies)
            {
                context.Companies.Add(c);
            }

            context.SaveChanges();

            var contacts = new Contact[]
            {
                new Contact{FirstName="Boo", LastName="Radley", CompanyId=1},
                new Contact{FirstName="Atticus", LastName="Finch", CompanyId=1},
                new Contact{FirstName="Samantha", LastName="Stephens", CompanyId=2},
                new Contact{FirstName="Larry", LastName="Tate", CompanyId=2},
                new Contact{FirstName="Darrin", LastName="Stephens", CompanyId=2},
                new Contact{FirstName="Vanellope", LastName="Von Schweetz", CompanyId=3},
                new Contact{FirstName="Taffyta", LastName="Muttonfudge", CompanyId=3},
                new Contact{FirstName="Sour", LastName="Bill", CompanyId=3},
                new Contact{FirstName="Felix", LastName="Fix-it", CompanyId=3}
            };

            foreach (Contact c in contacts)
            {
                context.Contacts.Add(c);
            }

            context.SaveChanges();
        }
    }
}
