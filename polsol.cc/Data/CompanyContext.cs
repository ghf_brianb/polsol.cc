﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using polsol.cc.Models;
using Microsoft.EntityFrameworkCore;

namespace polsol.cc.Data
{
    public class CompanyContext: DbContext
    {
        public CompanyContext(DbContextOptions<CompanyContext> options) : base (options)
        {
        }

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Company> Companies { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>().ToTable("Contact");
            modelBuilder.Entity<Company>().ToTable("Company");
        }
    }
}
