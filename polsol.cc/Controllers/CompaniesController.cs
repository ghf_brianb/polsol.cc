﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using polsol.cc.Data;
using polsol.cc.Models;

namespace polsol.cc.Controllers
{
    public class CompaniesController : Controller
    {
        private readonly CompanyContext _context;

        public CompaniesController(CompanyContext context)
        {
            _context = context;
        }

        // GET: Companies
        public async Task<IActionResult> Index(string sortOrder, string searchString)
        {
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["CatSortParm"] = sortOrder =="cat" ? "cat_desc" : "cat";
            ViewData["CurrentFilter"] = searchString;

            var companies = from c in _context.Companies
                            select c;

            if (!String.IsNullOrEmpty(searchString))
            {
                companies = companies.Where(c => c.PhoneNumber.Contains(searchString));
            }

            switch(sortOrder)
            {
                case "name_desc":
                    companies = companies.OrderByDescending(c => c.Name);
                    break;
                case "cat":
                    companies = companies.OrderBy(c => c.Catagory);
                    break;
                case "cat_desc":
                    companies = companies.OrderByDescending(c => c.Catagory);
                    break;
                default:
                    companies = companies.OrderBy(s => s.Name);
                    break;
            }

            return View(await companies.AsNoTracking().ToListAsync());
        }

        // GET: Companies/Details/5
        public async Task<IActionResult> Details(int? id, string sortOrder)
        {
            ViewData["FirstNameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "first_desc" : "";
            ViewData["LastNameSortParm"] = sortOrder == "last" ? "last_desc" : "last";

            if (id == null)
            {
                return NotFound();
            }

            var company = await _context.Companies
                .Include(c => c.Contacts)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.ID == id);
            if (company == null)
            {
                return NotFound();
            }

            switch(sortOrder)
            {
                case "first_desc":
                    company.Contacts = company.Contacts.OrderByDescending(c => c.FirstName).ToList();
                    break;
                case "last":
                    company.Contacts = company.Contacts.OrderBy(c => c.LastName).ToList();
                    break;
                case "last_desc":
                    company.Contacts = company.Contacts.OrderByDescending(c => c.LastName).ToList();
                    break;
                default:
                    company.Contacts = company.Contacts.OrderBy(c => c.FirstName).ToList();
                    break;
            }

            return View(company);
        }

        // GET: Companies/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Companies/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,FormattedPhoneNumber,Catagory")] Company company)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(company);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch ( DbUpdateException)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                            "Try again, and if the problem persists " +
                            "see your system administrator.");
            }            
            return View(company);
        }

        // GET: Companies/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var company = await _context.Companies.SingleOrDefaultAsync(m => m.ID == id);
            if (company == null)
            {
                return NotFound();
            }
            return View(company);
        }

        // POST: Companies/Edit/5
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPost(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyToUpdate = await _context.Companies.SingleOrDefaultAsync(s => s.ID == id);
            if (await TryUpdateModelAsync<Company>(
                companyToUpdate,
                "",
                c => c.Name, c => c.PhoneNumber, cc => cc.Catagory))
            {
                try
                {
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateException /* ex */)
                {
                    //Log the error (uncomment ex variable name and write a log.)
                    ModelState.AddModelError("", "Unable to save changes. " +
                        "Try again, and if the problem persists, " +
                        "see your system administrator.");
                }
            }
            return View(companyToUpdate);
        }

        // GET: Companies/Delete/5
        public async Task<IActionResult> Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return NotFound();
            }

            var company = await _context.Companies
                .Include(c => c.Contacts)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.ID == id);
            if (company == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ViewData["ErrorMessage"] =
                    "Delete failed. Try again, and if the problem persists " +
                    "see your system administrator.";
            }

            return View(company);
        }

        // POST: Companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var company = await _context.Companies
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.ID == id);
            if (company == null)
            {
                return RedirectToAction(nameof(Index));
            }

            try
            {
                _context.Companies.Remove(company);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {
                return RedirectToAction(nameof(Delete), new { id = id, saveChangesError = true });
            }
            
        }

        private bool CompanyExists(int id)
        {
            return _context.Companies.Any(e => e.ID == id);
        }
    }
}
